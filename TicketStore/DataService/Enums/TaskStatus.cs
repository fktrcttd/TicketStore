﻿namespace TicketStore.DataService.Models
{
    public enum TaskStatus
    {
        Assigned,
        Done,
        InProgress
    }
}